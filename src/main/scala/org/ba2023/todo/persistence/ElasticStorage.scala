package org.ba2023.todo.persistence

import cats.MonadError
import cats.effect.Async
import org.ba2023.todo.model.Todo
import com.sksamuel.elastic4s
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.Index
import cats.syntax.flatMap._
import cats.syntax.functor._
import org.ba2023.todo.persistence.TodoElasticStorage.TodoIndex
import org.ba2023.todo.persistence.errors.ElasticStoreError

trait TodoElasticStorage[F[_]] {
  def findTodos(query: String): F[Seq[Todo]]

  def storeTodo(todo: Todo): F[Unit]

  def init: F[Unit]
}

object TodoElasticStorage {
  val TodoIndex: Index = Index("todo")
}

final class TodoElasticStorageImpl[F[_]: Async: elastic4s.Functor: elastic4s.Executor](
  elasticClient: TodoElasticClient[F]
) extends TodoElasticStorage[F] {
  override def findTodos(query: String): F[Seq[Todo]] =
    elasticClient
      .execute(search(TodoIndex).query(query))
      .flatMap(data => Async[F].delay(data.result.to[Todo]))

  override def storeTodo(todo: Todo): F[Unit] =
    elasticClient
      .execute(
        indexInto(TodoIndex).fields(
          "id"          -> todo.id.toString,
          "title"       -> todo.title,
          "priority"    -> todo.priority,
          "dueDate"     -> todo.dueDate.toString,
          "owner"       -> todo.owner.toString,
          "description" -> todo.description
        )
      )
      .flatMap(resp => if (resp.isSuccess) Async[F].unit else MonadError[F, Throwable].raiseError(ElasticStoreError()))

  override def init: F[Unit] = elasticClient.execute(createIndex(TodoIndex.name)).void
}
