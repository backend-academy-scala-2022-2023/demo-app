package org.ba2023.todo.component

import cats.effect.kernel.Resource.ExitCase
import cats.effect.std.UUIDGen
import cats.effect.{IO, Resource}
import com.typesafe.scalalogging.StrictLogging
import org.ba2023.todo.core.AppCtx.WithMyContext
import org.ba2023.todo.core.SimpleRolePolicy
import org.ba2023.todo.http.Api

object ApiComponent extends StrictLogging {
  def init(implicit storageComponent: StorageComponent, metrics: Metrics): Resource[IO, Api] = {
    import storageComponent.{elasticStorage, todoEventSender, todoStorage, userStorage}
    Resource.makeCase(
      IO(
        new Api(
          todoStorage,
          elasticStorage,
          todoEventSender,
          userStorage,
          new SimpleRolePolicy[WithMyContext],
          UUIDGen[WithMyContext],
          metrics
        )
      )
    ) { (api, reason) =>
      (reason match {
        case ExitCase.Succeeded  => IO(logger.info("Complete"))
        case ExitCase.Errored(e) => IO(logger.error(e.getMessage, e))
        case ExitCase.Canceled   => IO(logger.warn("Stopped"))
      })
    }
  }
}
