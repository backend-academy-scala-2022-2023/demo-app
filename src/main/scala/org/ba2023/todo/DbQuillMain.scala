package org.ba2023.todo

import cats.effect.kernel.Resource
import cats.effect.{IO, IOApp}
import doobie.h2.H2Transactor
import doobie.implicits._
import doobie.{ExecutionContexts, Fragments}
import io.getquill.Literal
import io.getquill.doobie.DoobieContext

object DbQuillMain extends IOApp.Simple {

  import Fragments._

  override def run: IO[Unit] = {
    case class Person(name: String)
    case class Person2(id: Option[Int], name: String, age: Int)

    val transactor: Resource[IO, H2Transactor[IO]] = for {
      ce <- ExecutionContexts.fixedThreadPool[IO](sys.runtime.availableProcessors() * 2)
      xa <- H2Transactor.newH2Transactor[IO]("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "", "", ce)
    } yield xa

    val dc = new DoobieContext.H2(Literal)
    import dc.{SqlInfixInterpolator => _, run => qrun, _}
//    import dc.compat._

    transactor.use { xa =>
      val init = for {
        _ <- sql"CREATE TABLE person (name varchar)".update.run
        _ <- sql"INSERT INTO person (name) VALUES ('Vasia'), ('Masha')".update.run
        _ <- sql"CREATE TABLE person2 (id SERIAL NOT NULL, name varchar, age integer)".update.run
        _ <- sql"INSERT INTO person2 (name, age) VALUES ('Vasia', 16), ('Masha', 25), ('Masha', 19)".update.run
      } yield ()

      val result  = qrun(quote(query[Person])).map(println)
      val result2 = qrun(quote(query[Person2])).map(println)

      def filterByAge(age: Int) = {
        val q = quote(query[Person2].filter(_.age >= lift(age)))
        qrun(q).map(println)
      }

      def addPerson(name: String, age: Int) = {
        val person = Person2(None, name, age)
        val q      = quote(query[Person2].insertValue(lift(person)))
        qrun(q)
      }

      def addPersonWithResult(name: String, age: Int) = {
        val person = Person2(None, name, age)
        val q      = quote(query[Person2].insertValue(lift(person)).returningGenerated(_.id))
        qrun(q).map(println)
      }

      for {
        _ <- init.transact(xa)
        _ <- result.transact(xa)
        _ <- result2.transact(xa)
        _ <- filterByAge(20).transact(xa)
        _ <- addPerson("Petia", 20).transact(xa)
        _ <- addPersonWithResult("Kolia", 20).transact(xa)
        _ <- result2.transact(xa)
      } yield ()
    }
  }
}
