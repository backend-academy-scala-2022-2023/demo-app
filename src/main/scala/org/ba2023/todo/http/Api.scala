package org.ba2023.todo.http

import cats.effect.{Async, Sync}
import cats.effect.std.UUIDGen
import cats.implicits.catsSyntaxApplicativeError
import com.typesafe.scalalogging.StrictLogging
import io.getquill.Literal
import io.getquill.doobie.DoobieContext
import org.ba2023.todo.component.Metrics
import org.ba2023.todo.core.AppCtx.WithMyContext
import org.ba2023.todo.core.RolePolicy
import org.ba2023.todo.model.{Role, Todo, User}
import org.ba2023.todo.persistence._
import sttp.tapir.model.UsernamePassword
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import tofu.Fibers
import tofu.syntax.start._

class Api(todoStorage: TodoStorage,
          todoElasticStorage: TodoElasticStorage[WithMyContext],
          todoEventSender: TodoEventSender[WithMyContext],
          userStorage: UserStorage[WithMyContext],
          rolePolicy: RolePolicy[WithMyContext],
          gen: UUIDGen[WithMyContext],
          metrics: Metrics
) extends StrictLogging {
  import cats.syntax.functor._
  import cats.syntax.flatMap._
  import cats.syntax.applicative._
  import cats.syntax.either._

  val dc = new DoobieContext.Postgres(Literal)
  import dc.{SqlInfixInterpolator => _}

  private def authLogic(cred: UsernamePassword) =
    userStorage.get(cred.username).map {
      case Some(user) if cred.password.contains(user.password) => Right(user)
      case _                                                   => Left("User not found or password wrong")
    }

  val getAll = Endpoints.getAll
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => _ =>
      rolePolicy.canReadTodo(user).flatMap {
        case true  => todoStorage.list(user.id).map(_.asRight)
        case false => s"Access denied".asLeft[Seq[Todo]].pure[WithMyContext]
      }

    }

  val getTodo = Endpoints.getTodo
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => id =>
      rolePolicy.canReadTodo(user).flatMap {
        case true =>
          todoStorage.find(id).map {
            case Some(todo) => Right(todo.description)
            case None       => Left(s"TODO id=$id not found")
          }
        case false => s"Access denied".asLeft[String].pure[WithMyContext]
      }
    }

  val removeTodo = Endpoints.removeTodo
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => id =>
      todoStorage.find(id).flatMap {
        case Some(todo) =>
          rolePolicy.canModifyTodo(user, todo).flatMap {
            case true  => todoStorage.delete(id).void.map(_.asRight[String])
            case false => s"Access denied".asLeft[Unit].pure[WithMyContext]
          }
        case None => s"TODO id=$id not found".asLeft[Unit].pure[WithMyContext]
      }
    }

  val createTodo = Endpoints.createTodo
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogicSuccess(user => { case (title, text, dueDate, priority, tags) =>
      for {
        id  <- gen.randomUUID
        todo = Todo(id, title, priority.getOrElse(0), dueDate, user.id, text)
        _   <- todoStorage.create(todo)
        _   <- todoElasticStorage.storeTodo(todo)
        _   <- todoEventSender.publish(todo).compile.drain.start.void
      } yield ()
    })

  val updateTodo = Endpoints.updateTodo
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic(user => { case (id, text, priority) =>
      todoStorage.update(id, priority, text).map {
        case 0 => Left(s"TODO id=$id not found")
        case _ => Right(s"TODO id=$id updated")
      }
    })

  val registerUser = Endpoints.registerUser
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { admin => newUser =>
      rolePolicy.canCreateUser(admin).flatMap {
        case true =>
          userStorage.put(newUser) >> userStorage.addRole(newUser, Role.Reader).map(_.asRight[String])
        case false =>
          s"Access denied".asLeft[Unit].pure[WithMyContext]
      }
    }

  val searchTodos = Endpoints.search
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { _ => searchString =>
      todoElasticStorage.findTodos(searchString).attempt.map(_.left.map(_.getMessage))
    }

  def listenPipe: fs2.Pipe[WithMyContext, String, String] = (_: fs2.Stream[WithMyContext, String]) =>
    todoEventSender.listen

  val websocket = Endpoints.websocket
    .serverLogicSuccess[WithMyContext](_ => Sync[WithMyContext].delay(listenPipe))

  val apiEndpoints = getAll :: getTodo :: removeTodo :: createTodo :: updateTodo :: searchTodos :: Nil

  val docEndpoints = SwaggerInterpreter().fromServerEndpoints[WithMyContext](apiEndpoints, "Todo", "0.1.0")

  val endpoints: List[ServerEndpoint[Any, WithMyContext]] =
    apiEndpoints ++ docEndpoints :+ metrics.tapirMetrics.metricsEndpoint
}
