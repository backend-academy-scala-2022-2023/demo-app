package org.ba2023.todo.persistence

import doobie.Transactor
import doobie.implicits._
import org.ba2023.todo.core.AppCtx.WithMyContext
import org.ba2023.todo.model.Todo
import org.ba2023.todo.persistence.TodoStorageImpl._
import tofu.syntax.monadic._

import java.util.UUID

trait TodoStorage {

  def all(): WithMyContext[Seq[Todo]]

  def create(todo: Todo): WithMyContext[Unit]

  def find(id: UUID): WithMyContext[Option[Todo]]

  def list(owner: UUID): WithMyContext[Seq[Todo]]

  def delete(id: UUID): WithMyContext[Int]

  def update(id: UUID, priority: Option[Int], text: String): WithMyContext[Int]
}

final class TodoStorageImpl(transactor: Transactor[WithMyContext]) extends TodoStorage {

  override def create(todo: Todo): WithMyContext[Unit] = createQuery(todo).transact(transactor).void

  override def delete(id: UUID): WithMyContext[Int] =
    deleteQuery(id).transact(transactor).map(_.intValue)

  override def find(id: UUID): WithMyContext[Option[Todo]] =
    findQuery(id).transact(transactor).map(_.headOption)

  override def list(owner: UUID): WithMyContext[Seq[Todo]] =
    listQuery(owner).transact(transactor).map(_.toSeq)

  override def update(id: UUID, priority: Option[Int], text: String): WithMyContext[Int] =
    updateQuery(id, priority, text).transact(transactor).map(_.intValue)

  override def all(): WithMyContext[Seq[Todo]] = allQuery.transact(transactor).map(_.toSeq)
}

object TodoStorageImpl {

  import io.getquill.Literal
  import io.getquill.doobie.DoobieContext

  val dc = new DoobieContext.Postgres(Literal)

  import dc._

  def allQuery = run(quote(query[Todo]))

  def createQuery(todo: Todo) = run(quote(query[Todo].insertValue(lift(todo))))

  def deleteQuery(id: UUID) = run(quote(query[Todo].filter(_.id == lift(id)).delete))

  def findQuery(id: UUID) =
    run(quote(query[Todo].filter(_.id == lift(id))))

  def listQuery(owner: UUID) = run(quote(query[Todo].filter(_.owner == lift(owner)))).map(_.toList)

  def updateQuery(id: UUID, priority: Option[Int], text: String) =
    run(quote {
      query[Todo]
        .filter(_.id == lift(id))
        .update(r => r.priority -> lift(priority).getOrElse(r.priority), r => r.description -> lift(text))
    })
}
