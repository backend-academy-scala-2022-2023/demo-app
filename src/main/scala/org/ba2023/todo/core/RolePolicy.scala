package org.ba2023.todo.core

import org.ba2023.todo.model.{Todo, User}

trait RolePolicy[F[_]] {
  def canCreateUser(user: User): F[Boolean]
  def canCreateTodo(user: User): F[Boolean]
  def canReadTodo(user: User): F[Boolean]
  def canModifyTodo(user: User, todo: Todo): F[Boolean]
}
