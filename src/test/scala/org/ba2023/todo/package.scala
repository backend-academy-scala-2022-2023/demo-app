package org.ba2023

import cats.effect.unsafe.IORuntime
import org.ba2023.todo.core.AppCtx
import org.ba2023.todo.core.AppCtx.WithMyContext

import java.util.UUID

package object todo {
  implicit class Ops[A](val io: WithMyContext[A]) extends AnyVal {
    def unwrap(implicit runtime: IORuntime): A = io.run(AppCtx(UUID.randomUUID().toString)).unsafeRunSync()
  }
}
