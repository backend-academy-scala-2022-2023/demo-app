package org.ba2023.todo.model

import enumeratum._
import io.circe.{Decoder, Encoder}
import sttp.tapir.Schema

sealed trait Role extends EnumEntry

object Role extends Enum[Role] {
  val values = findValues
  case object Admin  extends Role // Может добавлять пользователей
  case object Editor extends Role // Может создавать TODO
  case object Reader extends Role // Может читать чужие, но не создавать

  implicit val e: Encoder[Role] = Encoder.encodeString.contramap(_.entryName)
  implicit val d: Decoder[Role] = Decoder.decodeString.emap(str =>
    Role.withNameOption(str) match {
      case Some(value) => Right(value)
      case None        => Left(s"Role $str nof found")
    }
  )
  implicit val s: Schema[Role] = Schema.derived
}
