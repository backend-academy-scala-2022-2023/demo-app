package org.ba2023.todo

import cats.effect.IO
import com.sksamuel.elastic4s.ElasticClient
import com.typesafe.scalalogging.StrictLogging
import doobie.util.transactor.Transactor
import org.ba2023.todo.component._
import org.ba2023.todo.config.AppConfig
import org.ba2023.todo.core.AppCtx.WithMyContext
import org.ba2023.todo.http.Api
import tofu.logging.Logs

class Application(implicit config: AppConfig) extends StrictLogging {

  def run: IO[Unit] = {
    val serverR = for {
      implicit0(logs: Logs[IO, WithMyContext])        <- LogsComponent.init()
      implicit0(metrics: Metrics)                     <- MetricsComponent.init()
      implicit0(dbComponent: DbComponent)             <- DbComponent.init
      implicit0(transactor: Transactor[WithMyContext]) = dbComponent.transactor
      implicit0(elasticClient: ElasticClient)          = dbComponent.elasticClient
      implicit0(storage: StorageComponent)            <- StorageComponent.init
      implicit0(api: Api)                             <- ApiComponent.init
      (server, ws)                                    <- ServerComponent.init
    } yield (server, ws)

    serverR.use { case (server, ws) =>
      for {
        _ <- IO(logger.info(s"Server started on http://${server.address.getHostName}:${server.address.getPort}"))
        _ <- IO(logger.info(s"Server started on http://${ws.address.getHostName}:${ws.address.getPort}"))
        _ <- IO.never[Unit]
      } yield ()
    }
  }
}
