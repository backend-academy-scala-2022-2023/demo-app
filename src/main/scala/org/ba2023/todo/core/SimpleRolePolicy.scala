package org.ba2023.todo.core

import cats.Applicative
import org.ba2023.todo.model.{Role, Todo, User}

class SimpleRolePolicy[F[_]: Applicative] extends RolePolicy[F] {
  import cats.syntax.applicative._
  override def canCreateUser(user: User): F[Boolean] = user.roles.contains(Role.Admin).pure

  override def canCreateTodo(user: User): F[Boolean] = user.roles.contains(Role.Editor).pure

  override def canReadTodo(user: User): F[Boolean] = user.roles.contains(Role.Reader).pure

  override def canModifyTodo(user: User, todo: Todo): F[Boolean] = (todo.owner == user.id).pure
}
