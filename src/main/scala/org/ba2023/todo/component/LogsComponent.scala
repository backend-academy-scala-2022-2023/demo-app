package org.ba2023.todo.component

import cats.effect.{IO, Resource}
import org.ba2023.todo.core.AppCtx.WithMyContext
import tofu.logging.Logs

object LogsComponent {

  def init(): Resource[IO, Logs[IO, WithMyContext]] = Resource.pure(Logs.withContext[IO, WithMyContext])

}
