package org.ba2023.todo

import cats.data.ReaderT
import cats.effect.std.UUIDGen
import cats.effect.unsafe.implicits.global
import com.dimafeng.testcontainers.scalatest.TestContainerForAll
import com.dimafeng.testcontainers.{JdbcDatabaseContainer, PostgreSQLContainer}
import doobie.implicits._
import doobie.postgres.implicits._
import doobie.util.transactor.Transactor
import io.circe.Printer
import io.circe.syntax.EncoderOps
import io.prometheus.client.CollectorRegistry
import org.ba2023.todo.component.Metrics
import org.ba2023.todo.core.AppCtx.WithMyContext
import org.ba2023.todo.core.SimpleRolePolicy
import org.ba2023.todo.http.Api
import org.ba2023.todo.model.{Role, Todo, User}
import org.ba2023.todo.persistence.{TodoElasticStorage, TodoEventSender, TodoStorageImpl, UserStorage, UserStorageImpl}
import org.flywaydb.core.Flyway
import org.scalamock.scalatest.MockFactory
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.testcontainers.utility.DockerImageName
import sttp.client3.testing.SttpBackendStub
import sttp.client3.{UriContext, basicRequest}
import sttp.tapir.integ.cats.CatsMonadError
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.server.metrics.prometheus.PrometheusMetrics
import sttp.tapir.server.stub.TapirStubInterpreter

import java.time.Instant
import java.util.UUID
import scala.util.Random

class ApiSpec extends AnyFlatSpec with Matchers with EitherValues with TestContainerForAll with MockFactory {
  import cats.syntax.flatMap._

  val imageName = DockerImageName.parse("postgres:15.2")
  override val containerDef = PostgreSQLContainer.Def(
    dockerImageName  = imageName,
    databaseName     = "testcontainer-scala",
    username         = "scala",
    password         = "scala",
    commonJdbcParams = JdbcDatabaseContainer.CommonParams().copy(initScriptPath = Some("init.db/init.sql"))
  )

  class Context(container: PostgreSQLContainer) {
    Class.forName(container.driverClassName)

    val flywayConfig = Flyway.configure
      .locations("migrations/")
      .dataSource(container.jdbcUrl, container.username, container.password)
    val flyway = flywayConfig.load()
    flyway.migrate()

    val collectorRegistry = new CollectorRegistry(true)
    val metrics           = PrometheusMetrics.default[WithMyContext]("http", collectorRegistry)

    val transactor =
      Transactor
        .fromDriverManager[WithMyContext](
          container.driverClassName,
          container.jdbcUrl,
          container.username,
          container.password
        )
    val interpreter    = TapirStubInterpreter(SttpBackendStub(new CatsMonadError[WithMyContext]()))
    val todoStorage    = new TodoStorageImpl(transactor)
    val userStorage    = stub[UserStorage[WithMyContext]]
    val policy         = new SimpleRolePolicy[WithMyContext]
    val gen            = UUIDGen[WithMyContext]
    val elasticStorage = stub[TodoElasticStorage[WithMyContext]]
    val eventSender    = stub[TodoEventSender[WithMyContext]]

    val api =
      new Api(todoStorage, elasticStorage, eventSender, userStorage, policy, gen, Metrics(collectorRegistry, metrics))

    val user = User(UUID.randomUUID(), "user", "pass", List(Role.Reader, Role.Editor))

    (eventSender.publish _).when(*).returns(fs2.Stream.unit)

    (elasticStorage.storeTodo _).when(*).returns(ReaderT.pure(()))

    (userStorage.get _)
      .when("user")
      .returns(ReaderT.pure(Option(user)))

    def populate(id: UUID, title: String, priority: Int, dueDate: Instant, owner: UUID, description: String) =
      sql"INSERT INTO todo (id, title, priority, dueDate, owner, description) VALUES ($id, $title, $priority, $dueDate, $owner, $description)".update.run
        .transact(transactor)

    def truncate = sql"truncate table todo".update.run.transact(transactor)

    def buildBackend[A, U, I, E, O, R](serverEndpoint: ServerEndpoint.Full[A, U, I, E, O, R, WithMyContext]) =
      interpreter.whenServerEndpoint(serverEndpoint).thenRunLogic().backend()
  }

  def fixtures(gen: UUIDGen[WithMyContext], user: User) = new {
    val id          = gen.randomUUID.unwrap
    val title       = "TODO#1"
    val priority    = 1
    val dueDate     = Instant.now().plusSeconds(Random.nextLong(500000))
    val owner       = user.id
    val description = "very specific description"
  }

  it should "return todo item" in {
    withContainers(new Context(_) {
      truncate.unwrap
      val backendStub = buildBackend(api.getTodo)
      val f           = fixtures(gen, user)
      import f._
      // when
      val response = populate(id, title, priority, dueDate, owner, description) >> basicRequest
        .get(uri"http://test.com/api/v1/todos/$id")
        .auth
        .basic(user.login, user.password)
        .send(backendStub)
      // then
      val result = response.unwrap
      println(result)
      result.body.value shouldBe description
    })
  }

  it should "return todo list" in {
    withContainers(new Context(_) {
      truncate.unwrap
      val backendStub = buildBackend(api.getAll)
      val f           = fixtures(gen, user)

      import f._
      // when
      val response = populate(id, title, priority, dueDate, owner, description) >> basicRequest
        .get(uri"http://test.com/api/v1/todos")
        .auth
        .basic(user.login, user.password)
        .send(backendStub)

      // then
      response.map { resp =>
        resp.body.value shouldBe List(Todo(id, title, priority, dueDate, owner, description)).asJson
          .printWith(Printer.noSpaces)
      }.unwrap
    })
  }

  it should "create todo" in {
    withContainers(new Context(_) {
      truncate.unwrap
      val backendStub = buildBackend(api.createTodo)
      val f           = fixtures(gen, user)
      import f._
      // when
      val response = basicRequest
        .post(uri"http://test.com/api/v1/todos?title=$title&text=$description&priority=$priority&dueDate=$dueDate")
        .auth
        .basic(user.login, user.password)
        .send(backendStub)

      val result = response.unwrap
      println(result)
      // then
      result.body.value shouldBe ""
    })
  }

  it should "delete todo" in {
    withContainers(new Context(_) {
      truncate.unwrap
      val backendStub = buildBackend(api.removeTodo)
      val f           = fixtures(gen, user)
      import f._
      // when
      val response = populate(id, title, priority, dueDate, owner, description) >> basicRequest
        .delete(uri"http://test.com/api/v1/todos/$id")
        .auth
        .basic(user.login, user.password)
        .send(backendStub)

      // then
      response.map(_.body.value shouldBe "").unwrap
    })
  }
}
