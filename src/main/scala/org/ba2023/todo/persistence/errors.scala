package org.ba2023.todo.persistence

object errors {

  final case class ElasticStoreError() extends Exception

}
