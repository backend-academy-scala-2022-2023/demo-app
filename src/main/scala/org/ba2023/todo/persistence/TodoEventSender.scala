package org.ba2023.todo.persistence

import cats.effect.kernel.Sync
import cats.~>
import dev.profunktor.redis4cats.data.RedisChannel
import dev.profunktor.redis4cats.pubsub.PubSubCommands
import io.circe.syntax.EncoderOps
import org.ba2023.todo.model.Todo

trait TodoEventSender[F[_]] {
  def listen: fs2.Stream[F, String]

  def publish(todo: Todo): fs2.Stream[F, Unit]
}

object TodoEventSender {
  val channel = RedisChannel("todo_create")
}

final class TodoEventSenderImpl[F[_]: Sync, I[_]: Sync](
  pubSubCommands: PubSubCommands[fs2.Stream[I, *], String, String]
)(fk: I ~> F)
  extends TodoEventSender[F] {
  override def listen: fs2.Stream[F, String] =
    pubSubCommands.subscribe(TodoEventSender.channel).translate[I, F](fk)

  override def publish(todo: Todo): fs2.Stream[F, Unit] =
    fs2.Stream
      .eval(Sync[I].delay(todo.asJson.noSpaces))
      .through(pubSubCommands.publish(TodoEventSender.channel))
      .translate(fk)

}
