package org.ba2023.todo.component

import cats.effect.{IO, Resource}
import cats.syntax.traverse._
import com.sksamuel.elastic4s.ElasticClient
import com.sksamuel.elastic4s.cats.effect.instances._
import dev.profunktor.redis4cats.connection.{RedisClient, RedisURI}
import dev.profunktor.redis4cats.data.RedisCodec
import dev.profunktor.redis4cats.effect.Log.NoOp._
import dev.profunktor.redis4cats.pubsub.{PubSub, PubSubCommands}
import doobie.util.transactor.Transactor
import fs2.Stream
import org.ba2023.todo.config.RedisConfig
import org.ba2023.todo.core.AppCtx.WithMyContext
import org.ba2023.todo.persistence._
import tofu.lift.Lift

final case class StorageComponent()(implicit
  val todoStorage: TodoStorage,
  val elasticStorage: TodoElasticStorage[WithMyContext],
  val todoEventSender: TodoEventSender[WithMyContext],
  val userStorage: UserStorage[WithMyContext]
)

object StorageComponent {
  def init(implicit
    xa: Transactor[WithMyContext],
    elasticClient: ElasticClient,
    redisConfig: RedisConfig
  ): Resource[IO, StorageComponent] =
    for {
      implicit0(client: TodoElasticClient[WithMyContext]) <-
        Resource.eval(IO.delay(new TodoElasticClientImpl[WithMyContext](elasticClient)))
      implicit0(userStorage: UserStorage[WithMyContext]) <-
        Resource.eval(IO.delay(new UserStorageImpl[WithMyContext](xa)))
      implicit0(elastic: TodoElasticStorage[WithMyContext]) <-
        Resource.eval(IO.delay(new TodoElasticStorageImpl[WithMyContext](client)))
      implicit0(storage: TodoStorage) <- Resource.eval(IO.delay(new TodoStorageImpl(xa)))
      uris                            <- Resource.eval(redisConfig.urls.map(RedisURI.make[IO](_)).sequence)
      redis                           <- RedisClient[IO].fromUri(uris.head)
      pubSub: PubSubCommands[Stream[IO, *], String, String] <-
        PubSub.mkPubSubConnection[IO, String, String](redis, RedisCodec.Utf8)
      implicit0(todoEventSender: TodoEventSender[WithMyContext]) <-
        Resource.eval(IO.delay(new TodoEventSenderImpl[WithMyContext, IO](pubSub)(Lift[IO, WithMyContext].liftF)))
    } yield StorageComponent()
}
