package org.ba2023.todo.http

import org.ba2023.todo.core.AppCtx.WithMyContext
import org.ba2023.todo.model.Todo
import org.ba2023.todo.model._
import sttp.tapir._
import sttp.tapir.json.circe._
import sttp.tapir.model.UsernamePassword

import java.time.Instant
import java.util.UUID

object Endpoints {
  /*
    GET /api/v1/todos - получить все
    POST /api/v1/todos - создать новый
    GET /api/v1/todos/:id - получить TODO с № ID
    POST /api/v1/todos/:id - обновить TODO с № ID
    DELETE /api/v1/todos/:id - удалить TODO с № ID
   */

  val apiV1Endpoint = endpoint
    .in("api" / "v1")
    .securityIn(auth.basic[UsernamePassword]())
    .errorOut(stringBody)

  val todosEndpoint = apiV1Endpoint.in("todos").tag("TODO")

  val usersEndpoint = apiV1Endpoint.in("users").tag("Users")

  private val text = query[String]("text")
    .description("Описание задачи")
    .example("Вынести мусор")
  private val priority = query[Option[Int]]("priority")
    .description("Приоритет задачи")
    .example(Option(2))
  private val tag = query[List[String]]("tag")
    .description("Метки")
    .example(List("покупки"))
  private val searchQuery = query[String]("query").description("Строка для поиска").example("give me something")

  private val title   = query[String]("title")
  private val dueDate = query[Instant]("dueDate")

  val getAll     = todosEndpoint.get.out(jsonBody[Seq[Todo]])
  val createTodo = todosEndpoint.post.in(title / text / dueDate / priority / tag)

  val todoEndpoint = todosEndpoint.in(path[UUID]("id"))

  val getTodo    = todoEndpoint.get.out(stringBody)
  val updateTodo = todoEndpoint.post.in(text / priority)

  val search = todosEndpoint.get.in("search").in(searchQuery).out(jsonBody[Seq[Todo]])

  val removeTodo = todoEndpoint.delete

  def websocket = endpoint.get
    .in("websocket")
    .out(
      webSocketBody[String, CodecFormat.TextPlain, String, CodecFormat.TextPlain](
        sttp.capabilities.fs2.Fs2Streams[WithMyContext]
      )
    )
  val registerUser = usersEndpoint.post.in(jsonBody[User])
}
