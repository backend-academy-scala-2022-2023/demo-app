val tapirVersion        = "1.2.8"
val quillVersion        = "4.6.0"
val doobieVersion       = "1.0.0-RC2"
val flywayVersion       = "9.15.2"
val pureconfigVersion   = "0.17.2"
val logbackVersion      = "1.3.3"
val emberVersion        = "0.23.18"
val scalaLoggingVersion = "3.9.4"
val tofuVersion         = "0.11.1"
val promVersion         = "0.16.0"
val elastic4sVersion    = "8.6.1"
val redis4catsVersion   = "1.4.1"
val enumeratumVersion   = "1.7.2"

val testcontainersScalaVersion = "0.40.12"

lazy val serverSettings = Seq(
  libraryDependencies ++= Seq(
    "com.softwaremill.sttp.tapir" %% "tapir-http4s-server"      % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle"  % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-json-circe"         % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-prometheus-metrics" % tapirVersion,
    "org.http4s"                  %% "http4s-ember-server"      % emberVersion,
    "ch.qos.logback"               % "logback-classic"          % logbackVersion,
    "com.typesafe.scala-logging"  %% "scala-logging"            % scalaLoggingVersion,
    "tf.tofu"                     %% "tofu-core-ce3"            % tofuVersion,
    "tf.tofu"                     %% "tofu-logging-derivation"  % tofuVersion,
    "tf.tofu"                     %% "tofu-logging-layout"      % tofuVersion,
    "tf.tofu"                     %% "tofu-logging-structured"  % tofuVersion,
    "io.prometheus"                % "simpleclient_common"      % promVersion,
    "io.prometheus"                % "simpleclient_hotspot"     % promVersion
  )
)
lazy val testSetting = Seq(
  libraryDependencies ++= Seq(
    "com.dimafeng"                %% "testcontainers-scala-scalatest"  % testcontainersScalaVersion,
    "com.dimafeng"                %% "testcontainers-scala-postgresql" % testcontainersScalaVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-sttp-stub-server"          % tapirVersion     % Test,
    "org.scalatest"               %% "scalatest"                       % "3.2.15"         % Test,
    "org.scalamock"               %% "scalamock"                       % "5.1.0"          % Test,
    "com.sksamuel.elastic4s"      %% "elastic4s-testkit"               % elastic4sVersion % Test
  )
)

lazy val dbSettings = Seq(
  libraryDependencies ++= Seq(
    "org.tpolecat" %% "doobie-core"      % doobieVersion,
    "org.tpolecat" %% "doobie-hikari"    % doobieVersion,
    "org.tpolecat" %% "doobie-postgres"  % doobieVersion,
    "org.tpolecat" %% "doobie-h2"        % doobieVersion,
    "org.tpolecat" %% "doobie-scalatest" % doobieVersion % Test,
    "org.flywaydb"  % "flyway-core"      % flywayVersion % Test,
    "io.getquill"  %% "quill-doobie"     % quillVersion
  )
)

lazy val utilsSettings = Seq(libraryDependencies ++= Seq("com.beachape" %% "enumeratum" % enumeratumVersion))

lazy val configSettings = Seq(
  libraryDependencies ++= Seq(
    "com.github.pureconfig" %% "pureconfig"             % pureconfigVersion,
    "com.github.pureconfig" %% "pureconfig-cats-effect" % pureconfigVersion
  )
)

lazy val elastic4sSettings = Seq(
  libraryDependencies ++= Seq(
    "com.sksamuel.elastic4s" %% "elastic4s-client-esjava" % elastic4sVersion,
    "com.sksamuel.elastic4s" %% "elastic4s-effect-cats"   % elastic4sVersion
  )
)

lazy val redisSettings = Seq(
  libraryDependencies ++= Seq(
    "dev.profunktor" %% "redis4cats-effects" % redis4catsVersion,
    "dev.profunktor" %% "redis4cats-streams" % redis4catsVersion
  )
)

lazy val compilerPluginsSettings = Seq(
  libraryDependencies ++= Seq(compilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"))
)

addCompilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full)

lazy val root = (project in file("."))
  .settings(name := "todo", scalaVersion := "2.13.10", version := "0.2.0-SNAPSHOT")
  .settings(
    serverSettings,
    testSetting,
    dbSettings,
    configSettings,
    compilerPluginsSettings,
    utilsSettings,
    elastic4sSettings,
    redisSettings
  )
  .settings(
    dockerExposedPorts := Seq(8080),
    dockerBaseImage := "eclipse-temurin:11",
    dockerEntrypoint := Seq("/opt/docker/bin/main")
  )
  .settings(Compile / unmanagedResourceDirectories ++= Seq((ThisBuild / baseDirectory).value / "sql"))
  .enablePlugins(DockerPlugin, JavaAppPackaging)
