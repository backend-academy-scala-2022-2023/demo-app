package org.ba2023.todo

import com.comcast.ip4s.Port
import pureconfig.ConfigReader

package object config {
  implicit val portReader: ConfigReader[Port] = ConfigReader.fromStringOpt(Port.fromString)
}
