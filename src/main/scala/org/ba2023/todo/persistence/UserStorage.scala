package org.ba2023.todo.persistence

import cats.effect.kernel.MonadCancelThrow
import doobie.Transactor
import doobie.implicits._
import io.getquill.SnakeCase
import org.ba2023.todo.model.{Role, User}

import java.util.UUID

trait UserStorage[F[_]] {
  def get(login: String): F[Option[User]]
  def put(user: User): F[Unit]
  def delete(user: User): F[Unit]

  def addRole(user: User, role: Role): F[Unit]
  def removeRole(user: User, role: Role): F[Unit]
}

class UserStorageImpl[F[_]: MonadCancelThrow](xa: Transactor[F]) extends UserStorage[F] {
  import UserStorageImpl._
  import cats.syntax.functor._
  override def get(login: String): F[Option[User]] =
    (for {
      user  <- findQuery(login)
      roles <- rolesQuery(login)
    } yield user.map(u => User(u.id, u.login, u.password, roles.map(_.name).flatMap(Role.withNameOption)))).transact(xa)

  override def put(user: User): F[Unit] = createQuery(user).transact(xa).void

  override def delete(user: User): F[Unit] = deleteQuery(user).transact(xa).void

  override def addRole(user: User, role: Role): F[Unit] = addRoleQuery(user, role).transact(xa)

  override def removeRole(user: User, role: Role): F[Unit] = removeRoleQuery(user, role).transact(xa)
}

object UserStorageImpl {

  case class UserRole(userId: UUID, roleId: Int)
  case class UserRecord(id: UUID, login: String, password: String)
  case class RoleRecord(id: Int, name: String)

  import io.getquill.Literal
  import io.getquill.doobie.DoobieContext

  val dc = new DoobieContext.Postgres(SnakeCase)

  import dc._

  val users = quote {
    querySchema[UserRecord]("users")
  }

  val roles = quote {
    querySchema[RoleRecord]("roles")
  }

  val userRoles = quote {
    querySchema[UserRole]("user_roles")
  }

  def findQuery(login: String) =
    run(quote(users.filter(_.login == lift(login)).take(1))).map(_.headOption)

  def findQueryWithRoles(login: String) = {
    val q = quote {
      for {
        user     <- users.filter(_.login == lift(login)).take(1)
        role     <- roles
        userRole <- userRoles if role.id == userRole.roleId && user.id == userRole.userId
      } yield (user, role)
    }

    run(q)
  }
  def createQuery(user: User) =
    run(quote(users.insert(_.login -> lift(user.login), _.password -> lift(user.password))))

  def deleteQuery(user: User) =
    run(quote(users.filter(_.login == lift(user.login)).delete))

  def rolesQuery(login: String) = {
    val q = quote {
      for {
        user     <- users.filter(_.login == lift(login))
        role     <- roles
        userRole <- userRoles if role.id == userRole.roleId && user.id == userRole.userId
      } yield role
    }

    run(q)
  }

  def addRoleQuery(user: User, role: Role) =
    for {
      userId <- run(quote(users.filter(_.login == lift(user.login)).map(_.id).take(1))).map(_.head)
      roleId <- run(quote(roles.insert(_.name -> lift(role.entryName)).onConflictIgnore.returning(_.id)))
      _      <- run(quote(userRoles.insert(_.roleId -> lift(roleId), _.userId -> lift(userId)).onConflictIgnore))
    } yield ()

  def removeRoleQuery(user: User, role: Role) =
    for {
      userId <- run(quote(users.filter(_.login == lift(user.login)).map(_.id).take(1))).map(_.head)
      roleId <- run(quote(roles.filter(_.name == lift(role.entryName)).map(_.id).take(1))).map(_.head)
      _      <- run(quote(userRoles.filter(r => r.roleId == lift(roleId) && r.userId == lift(userId))).delete)
    } yield ()
}
