package org.ba2023.todo.component

import cats.effect.{IO, Resource}
import com.sksamuel.elastic4s.http.JavaClient
import com.sksamuel.elastic4s.{ElasticClient, ElasticProperties}
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.metrics.prometheus.PrometheusMetricsTrackerFactory
import doobie.Transactor
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.ba2023.todo.config._
import org.ba2023.todo.core.AppCtx.WithMyContext
import org.elasticsearch.client.RestClientBuilder.HttpClientConfigCallback
import tofu.lift.Lift

final case class DbComponent()(implicit val transactor: Transactor[WithMyContext], val elasticClient: ElasticClient)

object DbComponent {
  def init(implicit
    config: DbConfig,
    elasticsearchConfig: ElasticsearchConfig,
    metrics: Metrics
  ): Resource[IO, DbComponent] =
    for {
      connEc <- ExecutionContexts.fixedThreadPool[IO](3)
      config <- Resource.eval(
                  IO.delay(new HikariConfig())
                    .flatTap { cfg =>
                      IO.delay {
                        cfg.setPassword(config.password)
                        cfg.setUsername(config.user)
                        cfg.setJdbcUrl(config.url)
                        cfg.setMetricsTrackerFactory(new PrometheusMetricsTrackerFactory(metrics.collectorRegistry))
                      }
                    }
                )
      implicit0(transactor: Transactor[WithMyContext]) <-
        HikariTransactor.fromHikariConfig[IO](config, connEc).map(_.mapK(Lift[IO, WithMyContext].liftF))
      implicit0(elastic: ElasticClient) <-
        Resource.make(
          IO.delay(
            ElasticClient(JavaClient(ElasticProperties(elasticsearchConfig.hosts), callback(elasticsearchConfig)))
          )
        )(cl => IO.delay(cl.close()))
    } yield DbComponent()

  def callback(config: ElasticsearchConfig) = new HttpClientConfigCallback {
    override def customizeHttpClient(httpClientBuilder: HttpAsyncClientBuilder): HttpAsyncClientBuilder = {
      val creds = new BasicCredentialsProvider()
      creds.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(config.username, config.password))
      httpClientBuilder.setDefaultCredentialsProvider(creds)
    }
  }

}
