package org.ba2023.todo.component

import cats.effect.std.UUIDGen
import cats.effect.{IO, Resource}
import cats.implicits.toSemigroupKOps
import com.comcast.ip4s.Host
import org.ba2023.todo.config.AppConfig
import org.ba2023.todo.core.AppCtx
import org.ba2023.todo.core.AppCtx.WithMyContext
import org.ba2023.todo.http.Api
import org.http4s.HttpRoutes
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits._
import org.http4s.server.Server
import org.http4s.server.websocket.WebSocketBuilder2
import sttp.tapir.server.http4s.{Http4sDefaultServerLog, Http4sServerInterpreter, Http4sServerOptions}
import tofu.WithRun
import tofu.lift.Lift
import tofu.logging.{Logging, Logs}

case class TodoHttpServer(routes: HttpRoutes[IO])

object ServerComponent {
  def init(implicit
    config: AppConfig,
    metrics: Metrics,
    api: Api,
    storageComponent: StorageComponent,
    logs: Logs[IO, WithMyContext]
  ): Resource[IO, (Server, Server)] =
    for {
      _          <- Resource.eval(runWithContext(storageComponent.elasticStorage.init))
      todoServer <- Resource.eval(buildHttp(metrics, api))
      wsServer   <- Resource.eval(buildWebSocket(api, metrics))
      run <-
        EmberServerBuilder
          .default[IO]
          .withHost(Host.fromString("0.0.0.0").get)
          .withPort(config.port)
          .withHttpWebSocketApp(wsb => (wsServer(wsb) <+> todoServer.routes).orNotFound)
          .build
      ws <-
        EmberServerBuilder
          .default[IO]
          .withHost(Host.fromString("0.0.0.0").get)
          .withPort(config.wsPort)
          .withHttpApp(todoServer.routes.orNotFound)
          .build
    } yield (run, ws)

  def serverLog(implicit logging: Logging[WithMyContext]) = Http4sDefaultServerLog[WithMyContext]
    .copy(logWhenReceived = true)
    .doLogWhenReceived(s => logging.info(s"Received request $s"))
    .doLogWhenHandled((s, exOpt) =>
      if (exOpt.isEmpty) logging.info(s"Request handled successfully $s")
      else logging.error(s"Failed to handle request $s", exOpt.get)
    )
    .doLogExceptions((s, err) => logging.error(s"Error occurred during request handling $s", err))
  def options(metrics: Metrics)(implicit logging: Logging[WithMyContext]) =
    Http4sServerOptions
      .customiseInterceptors[WithMyContext]
      .serverLog(serverLog)
      .metricsInterceptor(metrics.tapirMetrics.metricsInterceptor())

  def buildWebSocket(api: Api, metrics: Metrics)(implicit
    logs: Logs[IO, WithMyContext]
  ): IO[WebSocketBuilder2[IO] => HttpRoutes[IO]] = Logs.provide[IO, WithMyContext](implicit logging =>
    (wsb: WebSocketBuilder2[IO]) =>
      Http4sServerInterpreter[WithMyContext](options(metrics).options)
        .toWebSocketRoutes(api.websocket)(
          wsb.imapK[WithMyContext](Lift[IO, WithMyContext].liftF)(tofu.syntax.funk.funK(runWithContext))
        )
        .translate[IO](tofu.syntax.funk.funK(runWithContext))(Lift[IO, WithMyContext].liftF)
  )

  def buildHttp(metrics: Metrics, api: Api)(implicit logs: Logs[IO, WithMyContext]) = Logs.provide[IO, WithMyContext] {
    implicit logging =>
      TodoHttpServer(
        api.endpoints
          .map(ep =>
            Http4sServerInterpreter[WithMyContext](options(metrics).options)
              .toRoutes(ep)
              .translate[IO](tofu.syntax.funk.funK(runWithContext))(Lift[IO, WithMyContext].liftF)
          )
          .reduce(_ <+> _)
      )
  }

  def runWithContext[A](comp: WithMyContext[A]): IO[A] =
    for {
      ctx    <- UUIDGen.randomString[IO].map(AppCtx(_))
      result <- WithRun[WithMyContext, IO, AppCtx].runContextK(ctx)(comp)
    } yield result
}
