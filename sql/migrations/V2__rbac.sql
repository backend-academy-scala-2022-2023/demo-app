create table users (
    id uuid primary key default gen_random_uuid(),
    login varchar(300) not null,
    password varchar(300) not null,
    UNIQUE(login)
);

create table roles (
    id serial primary key,
    name varchar(300) not null,
    UNIQUE(name)
);

create table user_roles (
    user_id uuid not null,
    role_id integer not null,
    UNIQUE(user_id, role_id),

    CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES users(id),
    CONSTRAINT fk_role FOREIGN KEY(role_id) REFERENCES roles(id)
);

insert into users (login, password) VALUES ('admin', '12345678');
insert into roles (name) VALUES ('admin');
insert into user_roles (user_id, role_id) VALUES (
    (select id from users where login = 'admin' limit 1),
    (select id from roles where name = 'admin' limit 1)
);