FROM sbtscala/scala-sbt:eclipse-temurin-17.0.4_1.7.1_3.2.0 as build0
WORKDIR /opt/todo
COPY . .
RUN sbt "compile; test"
RUN sbt "Docker / stage"

FROM eclipse-temurin:11
EXPOSE 8080
WORKDIR /opt/todo
COPY --from=build0 /opt/todo/target/docker/stage/2/opt/docker/lib lib
COPY --from=build0 /opt/todo/target/docker/stage/4/opt/docker/lib lib
COPY --from=build0 /opt/todo/target/docker/stage/4/opt/docker/bin bin
ENTRYPOINT ["bash", "/opt/todo/bin/todo"]