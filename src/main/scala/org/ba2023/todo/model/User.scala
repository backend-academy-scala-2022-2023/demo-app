package org.ba2023.todo.model

import io.circe.{Decoder, Encoder}
import sttp.tapir.Schema

import java.util.UUID

case class User(id: UUID, login: String, password: String, roles: List[Role])

object User {
  implicit val e: Encoder[User] = io.circe.generic.semiauto.deriveEncoder
  implicit val d: Decoder[User] = io.circe.generic.semiauto.deriveDecoder
  implicit val s: Schema[User]  = Schema.derived
}
