 create table todos (id SERIAL PRIMARY KEY, text VARCHAR NOT NULL default '', priority integer default 0,
tags varchar[]);

insert into todos (text, priority, tags) VALUES
('TODO 0', 1, array[]::varchar[]),
('TODO 1', 1, array[]::varchar[]),
('TODO 2', 1, array[]::varchar[]),
('TODO 3', 1, array[]::varchar[]);