create table todo (
    id uuid primary key,
    title varchar(300),
    priority integer,
    dueDate timestamp with time zone,
    owner uuid,
    description text
);