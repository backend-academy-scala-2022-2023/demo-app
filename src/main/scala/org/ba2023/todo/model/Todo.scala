package org.ba2023.todo.model

import com.sksamuel.elastic4s.{Hit, HitReader}
import io.circe.parser
import io.circe.{Decoder, Encoder}
import sttp.tapir.Schema

import java.time.Instant
import java.util.UUID
import scala.util.Try

case class Todo(id: UUID, title: String, priority: Int, dueDate: Instant, owner: UUID, description: String)

object Todo {
  implicit val e: Encoder[Todo] = io.circe.generic.semiauto.deriveEncoder
  implicit val d: Decoder[Todo] = io.circe.generic.semiauto.deriveDecoder
  implicit val s: Schema[Todo]  = Schema.derived
  implicit val hitReader: HitReader[Todo] = new HitReader[Todo] {
    override def read(hit: Hit): Try[Todo] = parser.parse(hit.sourceAsString).toTry.flatMap(_.as[Todo].toTry)
  }
}
