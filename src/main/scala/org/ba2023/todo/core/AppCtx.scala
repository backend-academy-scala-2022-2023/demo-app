package org.ba2023.todo.core

import cats.data.ReaderT
import cats.effect.IO
import tofu.logging.{Loggable, LoggableContext}

case class AppCtx(traceId: String)
object AppCtx {
  type WithMyContext[A] = ReaderT[IO, AppCtx, A]

  implicit val loggable: Loggable[AppCtx]                      = tofu.logging.derivation.loggable.instance
  implicit val loggableContext: LoggableContext[WithMyContext] = LoggableContext.of[WithMyContext].instance
}
