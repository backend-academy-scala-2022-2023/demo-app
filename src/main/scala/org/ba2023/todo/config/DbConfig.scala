package org.ba2023.todo.config

case class DbConfig(url: String, user: String, password: String)

object DbConfig {
  implicit def dbConfigExt(implicit config: AppConfig): DbConfig = config.db
}

case class ElasticsearchConfig(hosts: String, username: String, password: String)

object ElasticsearchConfig {
  implicit def elasticsearchConfigExt(implicit config: AppConfig): ElasticsearchConfig = config.elastic
}

case class RedisConfig(urls: Seq[String])

object RedisConfig {
  implicit def redisConfig(implicit config: AppConfig): RedisConfig = config.redis
}
