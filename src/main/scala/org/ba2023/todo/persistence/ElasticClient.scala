package org.ba2023.todo.persistence

import com.sksamuel.elastic4s
import com.sksamuel.elastic4s.{ElasticClient, Handler, Response}

trait TodoElasticClient[F[_]] {
  def execute[T, U](t: T)(implicit handler: Handler[T, U], manifest: Manifest[U]): F[Response[U]]
}

class TodoElasticClientImpl[F[_]: elastic4s.Executor: elastic4s.Functor](client: ElasticClient)
  extends TodoElasticClient[F] {
  override def execute[T, U](t: T)(implicit handler: Handler[T, U], manifest: Manifest[U]): F[Response[U]] =
    client.execute(t)
}
