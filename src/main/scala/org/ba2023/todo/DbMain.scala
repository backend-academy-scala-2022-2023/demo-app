package org.ba2023.todo

import cats.effect.kernel.Resource
import cats.effect.{IO, IOApp}
import doobie.{ExecutionContexts, Fragments}
import doobie.h2.H2Transactor
import doobie.implicits._

object DbMain extends IOApp.Simple {
  import Fragments._
  override def run: IO[Unit] = {
    val transactor: Resource[IO, H2Transactor[IO]] = for {
      ce <- ExecutionContexts.fixedThreadPool[IO](sys.runtime.availableProcessors() * 2)
      xa <- H2Transactor.newH2Transactor[IO]("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "", "", ce)
    } yield xa

    transactor.use { xa =>
      val result: doobie.ConnectionIO[Unit] = for {
        _     <- sql"CREATE TABLE person (name varchar)".update.run
        _     <- sql"INSERT INTO person (name) VALUES ('Vasia'), ('Masha')".update.run
        names <- sql"SELECT name FROM person".query[String].to[List]
      } yield println(names)

      val result2: doobie.ConnectionIO[Unit] = for {
        _     <- sql"CREATE TABLE person2 (name varchar, age integer)".update.run
        _     <- sql"INSERT INTO person2 (name, age) VALUES ('Vasia', 16), ('Masha', 25), ('Masha', 19)".update.run
        names <- sql"SELECT name, age FROM person2".query[(String, Int)].to[List]
      } yield println(names)

      def filterByAge(age: Int): doobie.ConnectionIO[Unit] =
        sql"SELECT name, age FROM person2 where age > $age".query[(String, Int)].to[List].map(println)

      def filterByName(name: String): doobie.ConnectionIO[Unit] =
        sql"""
              SELECT name, age
              FROM person2
              WHERE name = $name
           """.query[(String, Int)].to[List].map(println)

      def whereName(name: String)    = fr"where name = $name"
      val selectPersonWithNameAndAge = fr"SELECT name, age FROM person2"

      def filterByNameV2(name: String): doobie.ConnectionIO[Unit] =
        sql"$selectPersonWithNameAndAge ${whereName(name)}".query[(String, Int)].to[List].map(println)

      def addPerson(name: String, age: Int) =
        sql"INSERT INTO person2 (name, age) VALUES ($name, $age)".update.run

      def getPeoples(name: Option[String], minAge: Option[Int]) = {
        val nameFr   = name.map(s => fr"name = $s")
        val minAgeFr = minAge.map(i => fr"age >= $i")

        (selectPersonWithNameAndAge ++ whereAndOpt(nameFr, minAgeFr))
          .query[(String, Int)]
          .to[List]
          .map(println)
      }

      for {
        _ <- result.transact(xa)
        _ <- result2.transact(xa)
        _ <- filterByAge(20).transact(xa)
        _ <- addPerson("Petia", 39).transact(xa)
        _ <- filterByName("Petia").transact(xa)
        _ <- filterByNameV2("Petia").transact(xa)
        _ <- getPeoples(None, None).transact(xa)
        _ <- getPeoples(None, Some(20)).transact(xa)
        _ <- getPeoples(Some("Masha"), Some(20)).transact(xa)
      } yield ()
    }
  }
}
