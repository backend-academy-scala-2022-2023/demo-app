package org.ba2023.todo.config

import com.comcast.ip4s.{IpLiteralSyntax, Port}

case class AppConfig(db: DbConfig,
                     elastic: ElasticsearchConfig,
                     redis: RedisConfig,
                     port: Port   = port"8080",
                     wsPort: Port = port"8081"
)
